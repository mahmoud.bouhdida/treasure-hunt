package com.carbonit.core.util;

import com.carbonit.model.Coordinates;
import com.carbonit.model.type.ActionType;
import com.carbonit.model.type.OrientationType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class SpaceUtilTest {

    private static Stream<Arguments> provideNexCoordinatesTestData() {
        return Stream.of(
                Arguments.of(
                        "Moving forward when orientation is EAST",
                        OrientationType.EAST, new Coordinates(2, 2), new Coordinates(3, 2)
                ),
                Arguments.of(
                        "Moving forward when orientation is WEST",
                        OrientationType.WEST, new Coordinates(2, 2), new Coordinates(1, 2)
                ),
                Arguments.of(
                        "Moving forward when orientation is EAST",
                        OrientationType.NORTH, new Coordinates(2, 2), new Coordinates(2, 1)
                ),
                Arguments.of(
                        "Moving forward when orientation is EAST",
                        OrientationType.SOUTH, new Coordinates(2, 2), new Coordinates(2, 3)
                )
        );
    }

    private static Stream<Arguments> provideNexOrientationTestData() {
        return Stream.of(
                Arguments.of(
                        "Turning left when orientation is EAST",
                        OrientationType.EAST, ActionType.TURN_LEFT, OrientationType.NORTH
                ),
                Arguments.of(
                        "Turning right when orientation is EAST",
                        OrientationType.EAST, ActionType.TURN_RIGHT, OrientationType.SOUTH
                ),
                Arguments.of(
                        "Turning left when orientation is WEST",
                        OrientationType.WEST, ActionType.TURN_LEFT, OrientationType.SOUTH
                ),
                Arguments.of(
                        "Turning right when orientation is WEST",
                        OrientationType.WEST, ActionType.TURN_RIGHT, OrientationType.NORTH
                ),
                Arguments.of(
                        "Turning left when orientation is NORTH",
                        OrientationType.NORTH, ActionType.TURN_LEFT, OrientationType.WEST
                ),
                Arguments.of(
                        "Turning right when orientation is NORTH",
                        OrientationType.NORTH, ActionType.TURN_RIGHT, OrientationType.EAST
                ),
                Arguments.of(
                        "Turning left when orientation is SOUTH",
                        OrientationType.SOUTH, ActionType.TURN_LEFT, OrientationType.EAST
                ),
                Arguments.of(
                        "Turning right when orientation is SOUTH",
                        OrientationType.SOUTH, ActionType.TURN_RIGHT, OrientationType.WEST
                )
        );
    }

    @ParameterizedTest(name = "TC{index}: {0}")
    @MethodSource("provideNexOrientationTestData")
    public void should_getNextCoordinatesForForwardAction_when_currentCoordinatesAndOrientationIsGiven(
            String displayName,
            OrientationType currentOrientation,
            ActionType turnAction,
            OrientationType expectedOrientation
    ) {
        Assertions.assertEquals(expectedOrientation, SpaceUtil.nextOrientation(currentOrientation, turnAction));
    }

    @ParameterizedTest(name = "TC{index}: {0}")
    @MethodSource("provideNexCoordinatesTestData")
    public void should_getNextOrientation_when_currentCoordinatesAndTurnActionIsGiven(
            String displayName,
            OrientationType orientation,
            Coordinates inputCoordinates,
            Coordinates expectedCoordinates
    ) {
        Assertions.assertEquals(expectedCoordinates, SpaceUtil.getDestinationCoordinates(inputCoordinates, orientation));
    }
}
