package com.carbonit.core;

import com.carbonit.model.AdventurerAction;
import com.carbonit.model.Coordinates;
import com.carbonit.model.MapData;
import com.carbonit.model.Size;
import com.carbonit.model.element.AdventurerMapElement;
import com.carbonit.model.element.MapElement;
import com.carbonit.model.element.MountainMapElement;
import com.carbonit.model.element.TreasureMapElement;
import com.carbonit.model.type.ActionType;
import com.carbonit.model.type.OrientationType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class StandardHuntStrategyTest {

    @Test
    public void should_generateActionsSequence_when_adventurersListIsProvided() {
        AdventurerMapElement adventurer1 = AdventurerMapElement.builder()
                .transitionSequence(
                        List.of(ActionType.MOVE_FORWARD, ActionType.MOVE_FORWARD, ActionType.TURN_RIGHT,
                                ActionType.MOVE_FORWARD, ActionType.TURN_RIGHT, ActionType.MOVE_FORWARD)
                )
                .build();
        AdventurerMapElement adventurer2 = AdventurerMapElement.builder()
                .transitionSequence(
                        List.of(ActionType.MOVE_FORWARD, ActionType.TURN_LEFT, ActionType.MOVE_FORWARD,
                                ActionType.TURN_RIGHT, ActionType.MOVE_FORWARD, ActionType.TURN_LEFT,
                                ActionType.TURN_RIGHT, ActionType.MOVE_FORWARD)
                )
                .build();
        AdventurerMapElement adventurer3 = AdventurerMapElement.builder()
                .transitionSequence(
                        List.of(ActionType.MOVE_FORWARD, ActionType.TURN_RIGHT, ActionType.TURN_LEFT,
                                ActionType.TURN_RIGHT, ActionType.MOVE_FORWARD, ActionType.TURN_LEFT)
                )
                .build();
        List<MapElement> adventurersList = List.of(
                adventurer1,
                adventurer2,
                adventurer3
        );

        List<AdventurerAction> expectedSequence = List.of(
                new AdventurerAction(adventurer1, ActionType.MOVE_FORWARD),
                new AdventurerAction(adventurer2, ActionType.MOVE_FORWARD),
                new AdventurerAction(adventurer3, ActionType.MOVE_FORWARD),

                new AdventurerAction(adventurer1, ActionType.MOVE_FORWARD),
                new AdventurerAction(adventurer2, ActionType.TURN_LEFT),
                new AdventurerAction(adventurer3, ActionType.TURN_RIGHT),

                new AdventurerAction(adventurer1, ActionType.TURN_RIGHT),
                new AdventurerAction(adventurer2, ActionType.MOVE_FORWARD),
                new AdventurerAction(adventurer3, ActionType.TURN_LEFT),

                new AdventurerAction(adventurer1, ActionType.MOVE_FORWARD),
                new AdventurerAction(adventurer2, ActionType.TURN_RIGHT),
                new AdventurerAction(adventurer3, ActionType.TURN_RIGHT),

                new AdventurerAction(adventurer1, ActionType.TURN_RIGHT),
                new AdventurerAction(adventurer2, ActionType.MOVE_FORWARD),
                new AdventurerAction(adventurer3, ActionType.MOVE_FORWARD),

                new AdventurerAction(adventurer1, ActionType.MOVE_FORWARD),
                new AdventurerAction(adventurer2, ActionType.TURN_LEFT),
                new AdventurerAction(adventurer3, ActionType.TURN_LEFT),

                new AdventurerAction(adventurer2, ActionType.TURN_RIGHT),

                new AdventurerAction(adventurer2, ActionType.MOVE_FORWARD)
        );

        StandardHuntStrategy huntStrategy = new StandardHuntStrategy();
        List<AdventurerAction> actualSequence = huntStrategy.getActionsSequence(adventurersList);

        Assertions.assertThat(actualSequence).isEqualTo(expectedSequence);
    }

    @Test
    public void should_huntTreasure_when_inputDataIsGiven() {
        MapData inputData = MapData.builder()
                .size(new Size(4, 3))
                .element(MountainMapElement.builder()
                        .coordinates(new Coordinates(1, 0))
                        .build())
                .element(MountainMapElement.builder()
                        .coordinates(new Coordinates(2, 1))
                        .build())
                .element(TreasureMapElement.builder()
                        .coordinates(new Coordinates(0, 3))
                        .treasureNumber(2)
                        .build())
                .element(TreasureMapElement.builder()
                        .coordinates(new Coordinates(1, 3))
                        .treasureNumber(3)
                        .build())
                .element(AdventurerMapElement.builder()
                        .coordinates(new Coordinates(1, 1))
                        .name("Lara")
                        .orientation(OrientationType.SOUTH)
                        .transitionSequence(List.of(
                                ActionType.MOVE_FORWARD, ActionType.MOVE_FORWARD, ActionType.TURN_RIGHT,
                                ActionType.MOVE_FORWARD, ActionType.TURN_RIGHT, ActionType.MOVE_FORWARD,
                                ActionType.TURN_LEFT, ActionType.TURN_LEFT, ActionType.MOVE_FORWARD))
                        .build())
                .build();

        HuntStrategy huntStrategy = new StandardHuntStrategy();
        MapData actualOutputData = huntStrategy.execute(inputData);

        MapData expectedOutputData = MapData.builder()
                .size(new Size(4, 3))
                .element(MountainMapElement.builder()
                        .coordinates(new Coordinates(1, 0))
                        .build())
                .element(MountainMapElement.builder()
                        .coordinates(new Coordinates(2, 1))
                        .build())
                .element(TreasureMapElement.builder()
                        .coordinates(new Coordinates(1, 3))
                        .treasureNumber(2)
                        .build())
                .element(AdventurerMapElement.builder()
                        .coordinates(new Coordinates(0, 3))
                        .name("Lara")
                        .orientation(OrientationType.SOUTH)
                        .capturedTreasures(3)
                        .build())
                .build();

        Assertions.assertThat(actualOutputData)
                .extracting(MapData::getElements)
                .asList()
                .usingRecursiveFieldByFieldElementComparatorIgnoringFields("transitionSequence")
                .containsExactlyInAnyOrderElementsOf(expectedOutputData.getElements());
    }
}
