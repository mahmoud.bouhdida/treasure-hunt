package com.carbonit.core.input;

import com.carbonit.model.Coordinates;
import com.carbonit.model.MapData;
import com.carbonit.model.Size;
import com.carbonit.model.element.AdventurerMapElement;
import com.carbonit.model.element.MountainMapElement;
import com.carbonit.model.element.TreasureMapElement;
import com.carbonit.model.type.ActionType;
import com.carbonit.model.type.OrientationType;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class FileInputDataAdapterTest {

    @Test
    public void should_parseInputDataFile_when_filePathIsGiven() {
        String inputFilePath = Optional.ofNullable(this.getClass().getResource("/input_test_1.txt"))
                .map(URL::getPath)
                .orElseThrow();
        MapData actualParsedData = new FileInputDataAdapter().readInputData(inputFilePath);

        MapData expectedData = MapData.builder()
                .size(new Size(4, 3))
                .element(MountainMapElement.builder()
                        .coordinates(new Coordinates(1, 0))
                        .build())
                .element(MountainMapElement.builder()
                        .coordinates(new Coordinates(2, 1))
                        .build())
                .element(TreasureMapElement.builder()
                        .coordinates(new Coordinates(0, 3))
                        .treasureNumber(2)
                        .build())
                .element(TreasureMapElement.builder()
                        .coordinates(new Coordinates(1, 3))
                        .treasureNumber(3)
                        .build())
                .element(AdventurerMapElement.builder()
                        .coordinates(new Coordinates(1, 1))
                        .name("Lara")
                        .orientation(OrientationType.SOUTH)
                        .transitionSequence(List.of(
                                ActionType.MOVE_FORWARD, ActionType.MOVE_FORWARD, ActionType.TURN_RIGHT,
                                ActionType.MOVE_FORWARD, ActionType.TURN_RIGHT, ActionType.MOVE_FORWARD,
                                ActionType.TURN_LEFT, ActionType.TURN_LEFT, ActionType.MOVE_FORWARD))
                        .build())
                .build();

        assertThat(actualParsedData).isEqualTo(expectedData);
    }
}
