package com.carbonit.core.output;

import com.carbonit.model.Coordinates;
import com.carbonit.model.MapData;
import com.carbonit.model.Size;
import com.carbonit.model.element.AdventurerMapElement;
import com.carbonit.model.element.MountainMapElement;
import com.carbonit.model.element.TreasureMapElement;
import com.carbonit.model.type.OrientationType;
import org.junit.jupiter.api.Test;

public class FileOutputDataAdapterTest {

    @Test
    public void should_exportMapDataToFile_when_mapDataGiven() {
        MapData dataToExport = MapData.builder()
                .size(new Size(4, 3))
                .element(MountainMapElement.builder()
                        .coordinates(new Coordinates(1, 0))
                        .build())
                .element(MountainMapElement.builder()
                        .coordinates(new Coordinates(2, 1))
                        .build())
                .element(TreasureMapElement.builder()
                        .coordinates(new Coordinates(1, 3))
                        .treasureNumber(2)
                        .build())
                .element(AdventurerMapElement.builder()
                        .coordinates(new Coordinates(0, 3))
                        .name("Lara")
                        .orientation(OrientationType.SOUTH)
                        .capturedTreasures(3)
                        .build())
                .build();

        new FileOutputDataAdapter().exportMapData(dataToExport);
    }
}
