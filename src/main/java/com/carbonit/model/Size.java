package com.carbonit.model;

public record Size(int vertical, int horizontal) {
}
