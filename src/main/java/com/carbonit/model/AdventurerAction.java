package com.carbonit.model;

import com.carbonit.model.element.AdventurerMapElement;
import com.carbonit.model.type.ActionType;

public record AdventurerAction(AdventurerMapElement adventurer, ActionType action) {
}
