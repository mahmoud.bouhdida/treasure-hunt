package com.carbonit.model;

import com.carbonit.model.element.MapElement;
import lombok.Builder;
import lombok.Singular;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class MapData {

    Size size;

    @Singular
    List<MapElement> elements;
}
