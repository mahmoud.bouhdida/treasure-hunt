package com.carbonit.model.element;

import com.carbonit.model.Coordinates;
import com.carbonit.model.type.ElementType;

public interface MapElement {

    ElementType getElementType();

    Coordinates getCoordinates();

    boolean isObstacle();

    void setCoordinates(Coordinates coordinates);
}
