package com.carbonit.model.element;

import com.carbonit.model.Coordinates;
import com.carbonit.model.type.ElementType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MountainMapElement implements MapElement{

    public static final ElementType ELEMENT_TYPE = ElementType.MOUNTAIN;

    Coordinates coordinates;
    @Builder.Default
    boolean obstacle = true;

    @Override
    public ElementType getElementType(){
        return ELEMENT_TYPE;
    }
}
