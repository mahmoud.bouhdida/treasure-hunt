package com.carbonit.model.element;

import com.carbonit.model.Coordinates;
import com.carbonit.model.type.ElementType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder(toBuilder = true)
public class TreasureMapElement implements MapElement{

    public static final ElementType ELEMENT_TYPE = ElementType.TREASURE;
    
    Coordinates coordinates;
    int treasureNumber;
    @Builder.Default
    boolean obstacle = false;

    @Override
    public ElementType getElementType(){
        return ELEMENT_TYPE;
    }
}
