package com.carbonit.model.element;

import com.carbonit.model.Coordinates;
import com.carbonit.model.type.ActionType;
import com.carbonit.model.type.ElementType;
import com.carbonit.model.type.OrientationType;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@Builder(toBuilder = true)
public class AdventurerMapElement implements MapElement {

    public static final ElementType ELEMENT_TYPE = ElementType.ADVENTURER;

    Coordinates coordinates;
    String name;
    List<ActionType> transitionSequence;
    @Builder.Default
    boolean obstacle = true;

    @EqualsAndHashCode.Exclude
    OrientationType orientation;

    @Builder.Default
    @EqualsAndHashCode.Exclude
    int capturedTreasures = 0;

    @Override
    public ElementType getElementType() {
        return ELEMENT_TYPE;
    }
}
