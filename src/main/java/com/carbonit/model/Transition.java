package com.carbonit.model;

public record Transition(Coordinates source, Coordinates destination) {
}
