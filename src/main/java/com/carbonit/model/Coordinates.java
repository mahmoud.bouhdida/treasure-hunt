package com.carbonit.model;

public record Coordinates(int x, int y) {
}
