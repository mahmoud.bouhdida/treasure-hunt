package com.carbonit.model.type;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.Arrays;
import java.util.Optional;

@AllArgsConstructor
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum ActionType {
    MOVE_FORWARD('A'),
    TURN_RIGHT('D'),
    TURN_LEFT('G');

    char code;

    public static Optional<ActionType> getByActionCode(char code) {
        return Arrays.stream(ActionType.values())
                .filter(actionType -> code == actionType.code)
                .findFirst();
    }
}
