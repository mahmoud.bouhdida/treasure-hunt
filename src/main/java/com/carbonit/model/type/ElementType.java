package com.carbonit.model.type;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.Arrays;
import java.util.Optional;

@AllArgsConstructor
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum ElementType {
    MOUNTAIN('M'),
    TREASURE('T'),
    ADVENTURER('A');

    char code;

    public static Optional<ElementType> getByElementCode(char code) {
        return Arrays.stream(ElementType.values())
                .filter(elementType -> code == elementType.code)
                .findFirst();
    }
}
