package com.carbonit.model.type;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.Arrays;
import java.util.Optional;

@AllArgsConstructor
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public enum OrientationType {
    EAST('E'),
    WEST('O'),
    NORTH('N'),
    SOUTH('S');

    char code;

    public static Optional<OrientationType> getByOrientationCode(char code) {
        return Arrays.stream(OrientationType.values())
                .filter(orientationType -> code == orientationType.code)
                .findFirst();
    }
}
