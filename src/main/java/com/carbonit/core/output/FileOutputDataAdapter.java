package com.carbonit.core.output;

import com.carbonit.model.MapData;
import com.carbonit.model.Size;
import com.carbonit.model.element.AdventurerMapElement;
import com.carbonit.model.element.MapElement;
import com.carbonit.model.element.TreasureMapElement;
import com.carbonit.model.type.ElementType;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Log4j2
public class FileOutputDataAdapter {
    public static final String OUTPUT_SEPARATOR = " - ";

    public String exportMapData(MapData mapData) {
        File outputData;
        try {
            outputData = File.createTempFile("treasure", "_output_data.txt");
            try (FileWriter fileWriter = new FileWriter(outputData);
                 BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

                bufferedWriter.write(getSizeString(mapData.getSize()));
                bufferedWriter.newLine();
                for (MapElement elt : mapData.getElements()) {
                    bufferedWriter.write(mapElementString(elt));
                    bufferedWriter.newLine();
                }
                bufferedWriter.flush();

                log.info("Data exported to {}", outputData.getAbsolutePath());
                return outputData.getAbsolutePath();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String mapElementString(MapElement mapElement) {
        return switch (mapElement.getElementType()) {
            case MOUNTAIN -> ElementType.MOUNTAIN.getCode() +
                    OUTPUT_SEPARATOR +
                    mapElement.getCoordinates().x() +
                    OUTPUT_SEPARATOR +
                    mapElement.getCoordinates().y();
            case TREASURE -> ElementType.TREASURE.getCode() +
                    OUTPUT_SEPARATOR +
                    mapElement.getCoordinates().x() +
                    OUTPUT_SEPARATOR +
                    mapElement.getCoordinates().y() +
                    OUTPUT_SEPARATOR +
                    ((TreasureMapElement) mapElement).getTreasureNumber();
            case ADVENTURER -> ElementType.ADVENTURER.getCode() +
                    OUTPUT_SEPARATOR +
                    ((AdventurerMapElement) mapElement).getName() +
                    OUTPUT_SEPARATOR +
                    mapElement.getCoordinates().x() +
                    OUTPUT_SEPARATOR +
                    mapElement.getCoordinates().y() +
                    OUTPUT_SEPARATOR +
                    ((AdventurerMapElement) mapElement).getOrientation().getCode() +
                    OUTPUT_SEPARATOR +
                    ((AdventurerMapElement) mapElement).getCapturedTreasures();
        };
    }

    private String getSizeString(Size size) {
        return "C" +
                OUTPUT_SEPARATOR +
                size.horizontal() +
                OUTPUT_SEPARATOR +
                size.vertical();
    }
}
