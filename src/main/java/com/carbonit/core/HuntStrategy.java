package com.carbonit.core;

import com.carbonit.model.MapData;

public interface HuntStrategy {

    /**
     * Executes the strategy according to the initial map data
     * @param initialState The map's initial data
     * @return The map's final state
     */
    MapData execute(MapData initialState);
}
