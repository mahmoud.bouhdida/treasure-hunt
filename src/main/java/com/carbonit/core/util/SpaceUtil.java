package com.carbonit.core.util;

import com.carbonit.model.Coordinates;
import com.carbonit.model.type.ActionType;
import com.carbonit.model.type.OrientationType;

/**
 * A 2-Dimensional space utility class
 */
public class SpaceUtil {

    /**
     * Get the next geographical orientation according to a turn action
     *
     * @param currentOrientation The current orientation
     * @param action             The left/right turn action
     * @return The next orientation
     */
    public static OrientationType nextOrientation(OrientationType currentOrientation, ActionType action) {
        return switch (currentOrientation) {
            case WEST -> action == ActionType.TURN_RIGHT ? OrientationType.NORTH : OrientationType.SOUTH;
            case EAST -> action == ActionType.TURN_RIGHT ? OrientationType.SOUTH : OrientationType.NORTH;
            case NORTH -> action == ActionType.TURN_RIGHT ? OrientationType.EAST : OrientationType.WEST;
            case SOUTH -> action == ActionType.TURN_RIGHT ? OrientationType.WEST : OrientationType.EAST;
        };
    }

    /**
     * Get the eventual new coordinates when moving forward from the initial coordinates
     *
     * @param initialCoordinates The initial coordinates
     * @param orientation        The orientation
     * @return The destination coordinates
     */
    public static Coordinates getDestinationCoordinates(Coordinates initialCoordinates, OrientationType orientation) {
        return switch (orientation) {
            case EAST -> new Coordinates(initialCoordinates.x() + 1, initialCoordinates.y());
            case WEST -> new Coordinates(initialCoordinates.x() - 1, initialCoordinates.y());
            case NORTH -> new Coordinates(initialCoordinates.x(), initialCoordinates.y() - 1);
            case SOUTH -> new Coordinates(initialCoordinates.x(), initialCoordinates.y() + 1);
        };
    }
}
