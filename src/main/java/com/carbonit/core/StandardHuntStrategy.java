package com.carbonit.core;

import com.carbonit.core.util.SpaceUtil;
import com.carbonit.model.*;
import com.carbonit.model.element.AdventurerMapElement;
import com.carbonit.model.element.MapElement;
import com.carbonit.model.element.TreasureMapElement;
import com.carbonit.model.type.ActionType;
import com.carbonit.model.type.ElementType;
import com.carbonit.model.type.OrientationType;
import lombok.extern.log4j.Log4j2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
public class StandardHuntStrategy implements HuntStrategy {

    private Map<Coordinates, List<MapElement>> mapState;
    private Size mapSize;

    @Override
    public MapData execute(MapData initialData) {
        mapSize = initialData.getSize();
        mapState = initialData.getElements()
                .stream()
                .collect(Collectors.toMap(MapElement::getCoordinates, Collections::singletonList));

        List<AdventurerAction> adventurerActions = getActionsSequence(initialData.getElements());
        for (AdventurerAction adventurerAction : adventurerActions) {
            if (adventurerAction.action() != ActionType.MOVE_FORWARD) {
                Map.Entry<Coordinates, List<MapElement>> currentAdventurerEntry =
                        getCurrentAdventurerEntry(adventurerAction.adventurer());

                AdventurerMapElement adventurerElement = currentAdventurerEntry.getValue()
                        .stream()
                        .filter(elt -> elt.getElementType() == ElementType.ADVENTURER)
                        .findFirst()
                        .map(elt -> (AdventurerMapElement) elt)
                        .orElseThrow();

                OrientationType newOrientation = SpaceUtil.nextOrientation(
                        adventurerElement.getOrientation(),
                        adventurerAction.action()
                );

                changeAdventurerOrientation(currentAdventurerEntry.getKey(), adventurerElement, newOrientation);

                log.info("Changed adventurer {} orientation from {} to {} with action {}",
                        adventurerAction.adventurer().getName(),
                        adventurerElement.getOrientation(),
                        newOrientation,
                        adventurerAction.action()
                );
            } else {
                nextAdventurerTransition(adventurerAction)
                        .ifPresent(this::applyTransition);
            }
        }

        return MapData.builder()
                .size(mapSize)
                .elements(
                        mapState.entrySet()
                                .stream()
                                .flatMap(entry -> entry.getValue().stream()
                                        .peek(elt -> elt.setCoordinates(entry.getKey())))
                                .toList()
                )
                .build();
    }

    private void changeAdventurerOrientation(
            Coordinates adventurerCoordinates,
            AdventurerMapElement adventurerElement,
            OrientationType newOrientation
    ) {
        mapState.replace(
                adventurerCoordinates,
                Stream.concat(
                        mapState.get(adventurerCoordinates)
                                .stream()
                                .filter(elt -> elt.getElementType() != ElementType.ADVENTURER),
                        Stream.of(adventurerElement.toBuilder()
                                .orientation(newOrientation)
                                .build())
                ).toList()
        );
    }

    private void applyTransition(Transition transition) {
        AdventurerMapElement adventurerElement = mapState.get(transition.source())
                .stream()
                .filter(elt -> elt.getElementType() == ElementType.ADVENTURER)
                .map(elt -> (AdventurerMapElement) elt)
                .findFirst()
                .orElseThrow();
        AdventurerMapElement.AdventurerMapElementBuilder adventurerBuilder =
                adventurerElement.toBuilder();

        log.info("Moving adventurer {} (heading {}) from {} to {}",
                adventurerElement.getName(),
                adventurerElement.getOrientation(),
                transition.source(),
                transition.destination());

        if (mapState.containsKey(transition.destination())) {
            mapState.get(transition.destination())
                    .stream()
                    .filter(elt -> elt.getElementType() == ElementType.TREASURE)
                    .map(elt -> (TreasureMapElement) elt)
                    .filter(elt -> elt.getTreasureNumber() > 0)
                    .findFirst()
                    .map(elt -> elt.toBuilder()
                            .treasureNumber(elt.getTreasureNumber() - 1)
                            .build())
                    .ifPresent(elt -> {
                        adventurerBuilder.capturedTreasures(adventurerElement.getCapturedTreasures() + 1);
                        log.info("{} captured a treasure @ {}", adventurerElement.getName(), transition.destination());

                        if (elt.getTreasureNumber() == 0) {
                            mapState.remove(transition.destination());
                            log.info("No more treasures @ {}", transition.destination());
                        } else {
                            mapState.replace(
                                    transition.destination(),
                                    Collections.singletonList(elt)
                            );
                        }
                    });
        }

        //Move adventurer to destination cell
        mapState.merge(
                transition.destination(),
                Collections.singletonList(adventurerBuilder.build()),
                (oldList, newList) -> Stream.of(oldList, newList).flatMap(List::stream).toList()
        );

        //Remove adventurer from source cell
        if (mapState.get(transition.source()).size() == 1) {
            mapState.remove(transition.source());
        } else {
            mapState.replace(
                    transition.source(),
                    mapState.get(transition.source())
                            .stream()
                            .filter(sourceElt -> sourceElt.getElementType() != ElementType.ADVENTURER)
                            .toList()
            );
        }
    }

    List<AdventurerAction> getActionsSequence(List<MapElement> mapElements) {
        List<AdventurerMapElement> adventurers =
                mapElements.stream()
                        .filter(elt -> elt.getElementType() == ElementType.ADVENTURER)
                        .map(elt -> (AdventurerMapElement) elt)
                        .toList();

        List<AdventurerAction> result = new ArrayList<>();
        int actionIdx = 0;
        int recentlyAddedActions;
        do {
            recentlyAddedActions = 0;
            for (AdventurerMapElement adventurer : adventurers) {
                if (adventurer.getTransitionSequence().size() > actionIdx) {
                    recentlyAddedActions++;
                    result.add(new AdventurerAction(adventurer, adventurer.getTransitionSequence().get(actionIdx)));
                }
            }
            actionIdx++;
        } while (recentlyAddedActions > 0);

        return result;
    }

    /*
    elt -> elt.getTransitionSequence()
                                .chars()
                                .mapToObj(c -> (char) c)
                                .map(ActionType::getByActionCode)
                                .filter(Optional::isPresent)
                                .map(Optional::get)
                                .toList()
     */

    private Optional<Transition> nextAdventurerTransition(AdventurerAction adventurerAction) {
        Map.Entry<Coordinates, List<MapElement>> adventurerEntry = getCurrentAdventurerEntry(adventurerAction.adventurer());
        Coordinates sourceCoordinates = adventurerEntry.getKey();

        OrientationType currentOrientation = adventurerEntry.getValue()
                .stream()
                .filter(elt -> elt.getElementType() == ElementType.ADVENTURER)
                .findFirst()
                .map(elt -> (AdventurerMapElement) elt)
                .map(AdventurerMapElement::getOrientation)
                .orElseThrow();

        Coordinates destinationCoordinates = SpaceUtil.getDestinationCoordinates(sourceCoordinates, currentOrientation);

        return Optional.of(destinationCoordinates)
                .filter(this::canMoveTo)
                .map(c -> new Transition(sourceCoordinates, c));
    }

    private Map.Entry<Coordinates, List<MapElement>> getCurrentAdventurerEntry(AdventurerMapElement adventurer) {
        return mapState.entrySet()
                .stream()
                .filter(entry -> entry.getValue().contains(adventurer))
                .findFirst()
                .orElseThrow();
    }

    private boolean canMoveTo(Coordinates coordinates) {
        return isCoordinatesInsideMap(coordinates) &&
                (!mapState.containsKey(coordinates) ||
                        mapState.get(coordinates).stream().noneMatch(MapElement::isObstacle));
    }

    private boolean isCoordinatesInsideMap(Coordinates coordinates) {
        return (Objects.nonNull(mapSize)) &&
                (Objects.nonNull(coordinates)) &&
                (coordinates.x() < mapSize.horizontal()) &&
                (coordinates.x() >= 0) &&
                (coordinates.y() < mapSize.vertical()) &&
                (coordinates.y() >= 0);
    }
}
