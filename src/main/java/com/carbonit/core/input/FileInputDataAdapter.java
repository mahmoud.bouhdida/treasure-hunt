package com.carbonit.core.input;

import com.carbonit.model.Coordinates;
import com.carbonit.model.MapData;
import com.carbonit.model.Size;
import com.carbonit.model.element.AdventurerMapElement;
import com.carbonit.model.element.MapElement;
import com.carbonit.model.element.MountainMapElement;
import com.carbonit.model.element.TreasureMapElement;
import com.carbonit.model.type.ActionType;
import com.carbonit.model.type.ElementType;
import com.carbonit.model.type.OrientationType;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Log4j2
public class FileInputDataAdapter {
    private static final String SPLIT_CHAR = "-";

    public MapData readInputData(String filePath) {
        log.info("Reading file path: {}", filePath);
        MapData.MapDataBuilder mapDataBuilder = MapData.builder();
        try (FileReader fileReader = new FileReader(filePath);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line = bufferedReader.readLine();

            while (line != null && !line.isEmpty()) {
                String finalLine = line;
                Optional<MapElement> mapElementOptional = ElementType.getByElementCode(line.charAt(0))
                        .map(elementType -> parseMapElementLine(finalLine, elementType));

                if (mapElementOptional.isPresent()) {
                    mapDataBuilder.element(mapElementOptional.get());
                } else if (Objects.equals(line.charAt(0), 'C')) {
                    List<String> lineParts = getLineParts(line);
                    mapDataBuilder.size(new Size(Integer.parseInt(lineParts.get(2)), Integer.parseInt(lineParts.get(1))));
                } else {
                    log.info("The line <{}> is ignored", line);
                }

                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            log.error("Error while reading input file", e);
            throw new IllegalArgumentException(e);
        }

        return mapDataBuilder.build();
    }

    MapElement parseMapElementLine(String line, ElementType elementType) {
        List<String> lineParts = getLineParts(line);
        return switch (elementType) {
            case MOUNTAIN -> MountainMapElement.builder()
                    .coordinates(new Coordinates(Integer.parseInt(lineParts.get(1)), Integer.parseInt(lineParts.get(2))))
                    .build();
            case TREASURE -> TreasureMapElement.builder()
                    .coordinates(new Coordinates(Integer.parseInt(lineParts.get(1)), Integer.parseInt(lineParts.get(2))))
                    .treasureNumber(Integer.parseInt(lineParts.get(3)))
                    .build();
            case ADVENTURER -> AdventurerMapElement.builder()
                    .coordinates(new Coordinates(Integer.parseInt(lineParts.get(2)), Integer.parseInt(lineParts.get(3))))
                    .name(lineParts.get(1))
                    .orientation(OrientationType.getByOrientationCode(lineParts.get(4).charAt(0)).orElseThrow())
                    .transitionSequence(
                            lineParts.get(5)
                                    .chars()
                                    .mapToObj(c -> (char) c)
                                    .map(ActionType::getByActionCode)
                                    .filter(Optional::isPresent)
                                    .map(Optional::get)
                                    .toList()
                    )
                    .build();
        };
    }

    List<String> getLineParts(String line) {
        return Arrays.stream(line.split(SPLIT_CHAR)).map(String::trim).toList();
    }
}
